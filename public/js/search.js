const search = document.querySelector('input[placeholder="search"]');
const projectContainer = document.querySelector(".projects");

search.addEventListener("keyup", function (event){
    if (event.key === "Enter") {
        event.preventDefault();

        const data = {search: this.value};

        fetch("/search", {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            body: JSON.stringify(data)
        }).then(function (response) {
            return response.json()
        }).then(function (books) {
            projectContainer.innerHTML = "";
            loadProjects(books)
        });
    }
});

function loadProjects(projects) {
    projects.forEach(book => {
        console.log(book);
        createProject(book);
    });
}

function createProject(book) {
    const template = document.querySelector("#project-template");

    const clone = template.content.cloneNode(true)

    const a = clone.querySelector("a");
    a.href = "/bookpage?id=" + book.id;
    const image = clone.querySelector("img");
    image.src = `/public/uploads/${book.image}`;
    const title = clone.querySelector("h2");
    title.innerHTML = book.title;
    const description = clone.querySelector("p");
    description.innerHTML = book.description;
    const rating = clone.querySelector("rating");
    description.innerHTML = book.rating;

    projectContainer.appendChild(clone);
}