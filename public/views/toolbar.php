<div class="base-container">
    <nav>
        <img src="public/img/logo.svg">
        <ul>
            <li>
                <i class="fas fa-home"></i>
                <a href="/projects" class="button">home</a>
            </li>
            <li>
                <i class="far fa-star"></i>
                <a href="#" class="button">top 100</a>
            </li>
            <li>
                <i class="fas fa-book-open"></i>
                <a href="#" class="button">my books</a>
            </li>
            <li>
                <i class="fas fa-list"></i>
                <a href="/browse" class="button">browse</a>
            </li>
            <li>
                <i class="fas fa-cog"></i>
                <a href="/settings" class="button">settings</a>
            </li>
        </ul>
    </nav>
    <main>
        <header>
            <div class="search-bar">

                <input placeholder="search">

            </div>
            <div class="log-out">
                <i class="fas fa-times"></i>
                <a href="/logout" class="exit">logout</a>

            </div>
        </header>