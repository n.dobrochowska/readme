<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <link rel="stylesheet" type="text/css" href="public/css/book.css">
    <script src="https://kit.fontawesome.com/482a217c1b.js" crossorigin="anonymous"></script>
    <title>BROWSE</title>
</head>
<body>
<?php include("toolbar.php") ?>
<section class="addbook">
    <div class="book-container">
        <h1> ADD BOOK</h1>
        <form class="book" action="addBookToDb" method="POST" ENCTYPE="multipart/form-data">
            <div class="messages">
                <?php
                if(isset($messages)){
                    foreach($messages as $message) {
                        echo $message;
                    }
                }
                ?>
            </div>

            <input name="title" type="text" placeholder="title">
            <textarea name="description" rows="5" placeholder="description"></textarea>

            <input type="file" name="file">

            <label for="cars">Choose an author:</label>
            <select id="author" name="author">
                <option value=""></option>
                <?php foreach ($authors as $author): ?>
                    <option><?= $author->getName(); ?> <?= $author->getSurname(); ?></option>
                <?php endforeach; ?>
            </select>
            <label for="cars">Category 1 (Required)</label>
            <select class="category" name="cat1">
                <option value=""></option>
                <?php foreach ($categories as $category): ?>
                    <option><?= $category->getType(); ?></option>
                <?php endforeach; ?>
            </select>
            <label for="cars">Category 2</label>
            <select class="category" name="cat2">
                <option value=""></option>
                <?php foreach ($categories as $category): ?>
                    <option><?= $category->getType(); ?></option>
                <?php endforeach; ?>
            </select>
            <label for="cars">Category 3</label>
            <select class="category" name="cat3">
                <option value=""></option>
                <?php foreach ($categories as $category): ?>
                    <option><?= $category->getType(); ?></option>
                <?php endforeach; ?>
            </select>
            <button type="submit">Add</button>
        </form>
    </div>
</section>
</main>
</div>
</body>