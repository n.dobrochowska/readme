<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <link rel="stylesheet" type="text/css" href="public/css/bookpage.css">
    <script src="https://kit.fontawesome.com/482a217c1b.js" crossorigin="anonymous"></script>

    <title>BOOKPAGE</title>
</head>
<body>
<?php include("toolbar.php") ?>
        <section class="bookpage">
            <div id="book" class="book_section">
                <img src="public/uploads/<?= $bookpage->getImage(); ?>">
                <div>
                    <div class="text">
                        <h2><?= $bookpage->getTitle(); ?></h2>
                        <h5>Autor:<?= $bookpage->getAuthor(); ?></h5>
                        <h6>category: <?= $bookpage->getCategory(); ?></h6>
                        <p><?= $bookpage->getDescription(); ?></p>
                    </div>
                    <div class="rating">
                        <i class="far fa-star">5.0</i>
                    </div>
                </div>
            </div>
            <div class="comment">
                <form method="POST" action="addComment" class="comment_form">
                    <textarea class="comment_box" type="text" placeholder="comment" name="comment"></textarea>
                    <input type="submit" value="send">
                </form>
            </div>
            <div class="comments_section">
                <div id="comment1" class="comments">
                        <div class="comment_text">
                            <h4>author</h4>
                            <p>comment_text</p>
                        </div>
                </div>
                <div id="comment2" class="comments">
                    <div class="comment_text">
                        <h4>author</h4>
                        <p>comment_text</p>
                    </div>
                </div>
            </div>
        </section>
    </main>
</div>
</body>