<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <link rel="stylesheet" type="text/css" href="public/css/projects.css">
    <script src="https://kit.fontawesome.com/482a217c1b.js" crossorigin="anonymous"></script>
    <script type="text/javascript" src="./public/js/search.js" defer></script>

    <title>PROJECTS</title>
</head>
<body>
    <?php include("toolbar.php") ?>
        <section class="projects">
            <?php foreach($books as $book): ?>
                <a href="/bookpage?id=<?= $book->getId(); ?>" class="book">
                <img src="public/uploads/<?= $book->getImage(); ?>">
                <div>
                    <div class="text">
                        <h2><?= $book->getTitle(); ?></h2>
                        <p><?= $book->getDescription(); ?></p>
                    </div>
                    <div class="rating">
                        <i class="far fa-star">5.0</i>
                    </div>
                </div>
            </a>
        <?php endforeach; ?>
        </section>
    </main>
</div>
</body>

<template id="project-template">
    <a href="" class="book">
        <img src="">
        <div>
            <div class="text">
                <h2>title</h2>
                <p>description</p>
            </div>
            <div class="rating">
                <i class="far fa-star">5.0</i>
            </div>
        </div>
    </a>
</template>