<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <link rel="stylesheet" type="text/css" href="public/css/browse.css">
    <script src="https://kit.fontawesome.com/482a217c1b.js" crossorigin="anonymous"></script>
    <title>BROWSE</title>
</head>
<body>
<?php include("toolbar.php") ?>
<section class="browse">
    <div id="cat1" class="cat">
        <a href="/addAuthor" class="buttoncat">Add Author</a>
    </div>
    <div id="cat2" class="cat">
        <a href="/addBook" class="buttoncat">Add Book</a>
    </div>
    <div id="cat3" class="cat">
        <a href="#" class="buttoncat">Block User</a>
    </div>
</section>
</main>
</div>
</body>