<!DOCTYPE html>
<head>
  <link rel="stylesheet" type="text/css" href="public/css/style.css">
  <link rel="stylesheet" type="text/css" href="public/css/browse.css">
  <script src="https://kit.fontawesome.com/482a217c1b.js" crossorigin="anonymous"></script>
  <title>BROWSE</title>
</head>
<body>
<?php include("toolbar.php") ?>
    <section class="browse">
      <div id="cat1" class="cat">
        <a href="#" class="buttoncat">Art</a>
      </div>
      <div id="cat2" class="cat">
        <a href="#" class="buttoncat">Biography</a>
      </div>
      <div id="cat3" class="cat">
        <a href="#" class="buttoncat">Classics</a>
      </div>
      <div id="cat4" class="cat">
        <a href="#" class="buttoncat">Comics</a>
      </div>
      <div id="cat5" class="cat">
        <a href="#" class="buttoncat">Fantasy</a>
      </div>
      <div id="cat6" class="cat">
        <a href="#" class="buttoncat">Fiction</a>
      </div>
      <div id="cat7" class="cat">
        <a href="#" class="buttoncat">History</a>
      </div>
      <div id="cat8" class="cat">
        <a href="#" class="buttoncat">Horror</a>
      </div>
      <div id="cat9" class="cat">
        <a href="#" class="buttoncat">Mystery</a>
      </div>
      <div id="cat10" class="cat">
        <a href="#" class="buttoncat">Poetry</a>
      </div>
      <div id="cat11" class="cat">
        <a href="#" class="buttoncat">Romance</a>
      </div>
      <div id="cat12" class="cat">
        <a href="#" class="buttoncat">Science Fiction</a>
      </div>
      <div id="cat13" class="cat">
        <a href="#" class="buttoncat">Thriller</a>
      </div>
      <div id="cat14" class="cat">
        <a href="#" class="buttoncat">Travel</a>
      </div>
      <div id="cat15" class="cat">
        <a href="#" class="buttoncat">Other</a>
      </div>
    </section>
  </main>
</div>
</body>