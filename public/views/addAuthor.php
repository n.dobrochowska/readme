<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <link rel="stylesheet" type="text/css" href="public/css/author.css">
    <script src="https://kit.fontawesome.com/482a217c1b.js" crossorigin="anonymous"></script>
    <title>BROWSE</title>
</head>
<body>
<?php include("toolbar.php") ?>
<section class="addauthor">
    <div class="author-container">
        <h1> ADD AUTHOR</h1>
        <form class="author" action="addAuthorToDb" method="POST">
            <div class="messages">
                <?php
                if(isset($messages)){
                    foreach($messages as $message) {
                        echo $message;
                    }
                }
                ?>
            </div>
            <input name="name" type="text" placeholder="name">
            <input name="surname" type="text" placeholder="surname">
            <button type="submit">Add</button>
        </form>
    </div>
</section>
</main>
</div>
</body>a