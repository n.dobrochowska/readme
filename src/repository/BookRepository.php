<?php

require_once 'Repository.php';
require_once __DIR__.'/../models/Book.php';

class BookRepository extends Repository
{
    public function getBooks(): array
    {
        $result = [];

        $stmt = $this->database->connect()->prepare('
            SELECT * FROM public.book
        ');
        $stmt->execute();

        $books = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($books as $book) {
            $result[] = new Book(
                $book['title'],
                $book['description'],
                $book['image'],
                $book['id_author'],
                $book['id']
            );
        }

        return $result;
    }


    public function getBook(int $id): ?Book
    {
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM public.book WHERE id = :id
        ');
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();

        $book = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($book == false) {
            return null; //mozna poprawic
        }

        return new Book(
            $book['title'],
            $book['description'],
            $book['image'],
            $book['id_author'],
            $book['id']
        );
    }

    public function getBookByTitle(string $title): ?Book
    {
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM public.book WHERE title = :title
        ');
        $stmt->bindParam(':title', $title, PDO::PARAM_STR);
        $stmt->execute();

        $book = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($book == false) {
            return null; //mozna poprawic
        }

        return new Book(
            $book['title'],
            $book['description'],
            $book['image'],
            $book['id_author'],
            $book['id']
        );
    }

    public function getBookId(string $title): ?int
    {
        $stmt = $this->database->connect()->prepare('
            SELECT id FROM public.book WHERE title = :title
        ');
        $stmt->bindParam(':title', $title, PDO::PARAM_STR);
        $stmt->execute();

        $book = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($book == false) {
            return null; //mozna poprawic
        }

        return $book['id'];
    }

    public function addBook (Book $book): void
    {
        $stmt = $this->database->connect()->prepare('
           INSERT INTO book (title, description, image, id_author)
           VALUES (?,?,?,?)
        ');

        $stmt->execute([
            $book->getTitle(),
            $book->getDescription(),
            $book->getImage(),
            $book->getIdAuthor()
        ]);
    }

}