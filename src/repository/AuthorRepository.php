<?php

require_once 'Repository.php';
require_once __DIR__.'/../models/Author.php';

class AuthorRepository extends Repository
{
    public function getAuthor(int $id): ?Author
    {
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM public.author WHERE id = :id
        ');
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();

        $author = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($author == false) {
            return null; //mozna poprawic
        }

        return new Author(
            $author['name'],
            $author['surname'],
            $author['id']
        );
    }

    public function getAuthorByName(string $name, string $surname): ?Author
    {
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM public.author WHERE name = :name AND surname = :surname
        ');
        $stmt->bindParam(':name', $name, PDO::PARAM_STR);
        $stmt->bindParam(':surname', $surname, PDO::PARAM_STR);
        $stmt->execute();

        $author = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($author == false) {
            return null; //mozna poprawic
        }

        return new Author(
            $author['name'],
            $author['surname'],
            $author['id']
        );
    }

    public function addAuthor (Author $author): void
    {
        $stmt = $this->database->connect()->prepare('
           INSERT INTO author (name, surname)
           VALUES (?,?)
        ');

        $stmt->execute([
          $author->getName(),
          $author->getSurname()
        ]);
    }

    public function getAuthors(): array
    {
        $result = [];

        $stmt = $this->database->connect()->prepare('
            SELECT * FROM public.author
        ');
        $stmt->execute();

        $authors = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($authors as $author) {
            $result[] = new Author(
                $author['name'],
                $author['surname'],
                $author['id']
            );
        }

        return $result;
    }

    public function getAuthorId(string $fullname): ?int
    {
        $table = explode(' ',$fullname);
        $name = $table[0];
        $surname = $table[1];
        $stmt = $this->database->connect()->prepare('
            SELECT id FROM public.author WHERE name = :name AND surname = :surname
        ');
        $stmt->bindParam(':name', $name, PDO::PARAM_STR);
        $stmt->bindParam(':surname', $surname, PDO::PARAM_STR);
        $stmt->execute();

        $author = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($author == false) {
            return null;
        }

        return $author['id'];
    }

}