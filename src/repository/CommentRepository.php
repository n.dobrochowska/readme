<?php

require_once 'Repository.php';
require_once __DIR__.'/../models/Comment.php';

class CommentRepository extends Repository
{
    public function getComment(int $id): ?Comment
    {
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM public.comment WHERE id = :id
        ');
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();

        $comment = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($comment == false) {
            return null; //mozna poprawic
        }

        return new Comment(
            $comment['id'],
            $comment['content'],
            $comment['date'],
            $comment['id_users'],
            $comment['id_book']
        );
    }
}