<?php

require_once 'Repository.php';
require_once __DIR__.'/../models/Bookpage.php';

class BookpageRepository extends Repository
{
    public function getBookpage(int $id): ?Bookpage
    {
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM bookpage WHERE id = :id
        ');
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();

        $bookpage = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($bookpage == false) {
            return null; //mozna poprawic
        }

        return new Bookpage(
            $bookpage['title'],
            $bookpage['description'],
            $bookpage['image'],
            $bookpage['name']." ".$bookpage['surname'],
            $bookpage['category']
        );
    }

    public function getBookByTitle(string $searchString){
        $searchString = '%'.strtolower($searchString).'%';

        $stmt = $this->database->connect()->prepare('
            SELECT * FROM book WHERE LOWER(title) LIKE :search
        ');
        $stmt->bindParam(':search', $searchString, PDO::PARAM_STR);
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
}