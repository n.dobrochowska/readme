<?php

require_once 'Repository.php';
require_once __DIR__.'/../models/Category.php';

class CategoryRepository extends Repository
{
    public function getCategories(): array
    {
        $result = [];

        $stmt = $this->database->connect()->prepare('
            SELECT * FROM public.category
        ');
        $stmt->execute();

        $categories = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($categories as $category) {
            $result[] = new Category(
                $category['type'],
                $category['id']
            );
        }

        return $result;
    }

    public function getCategoryId(string $type): ?int
    {

        $stmt = $this->database->connect()->prepare('
            SELECT id FROM public.category WHERE type = :type
        ');
        $stmt->bindParam(':type', $type, PDO::PARAM_STR);
        $stmt->execute();

        $cat = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($cat == false) {
            return null;
        }

        return $cat['id'];
    }

    public function addBookToCategory (string $type, int $book_id): void
    {
        $type_id = $this->getCategoryId($type);

        $stmt = $this->database->connect()->prepare('
           INSERT INTO book_category (id_book, id_category)
           VALUES (?,?)
        ');

        $stmt->execute([
            $book_id,
            $type_id
        ]);
    }

}