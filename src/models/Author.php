<?php


class Author
{
    private $name;
    private $surname;
    private $id;

    public function __construct(string $name,string $surname, int $id)
    {
        $this->name = $name;
        $this->surname = $surname;
        $this->id = $id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getSurname(): string
    {
        return $this->surname;
    }

    public function setSurname(string $surname): void
    {
        $this->surname = $surname;
    }

    public function getInt()
    {
        return $this->int;
    }

    public function setInt($int): void
    {
        $this->int = $int;
    }



}