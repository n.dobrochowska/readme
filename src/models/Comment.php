<?php
class Comment
{
    private $content;
    private $date;
    private $id_users;
    private $id_book;
    private $id;

    public function __construct(int $id, string $content, string $date, int $id_users, int $id_book)
    {
        $this->id = $id;
        $this->content = $content;
        $this->date = $date;
        $this->id_users = $id_users;
        $this->id_book = $id_book;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function setContent(string $content): void
    {
        $this->content = $content;
    }

    public function getDate(): string
    {
        return $this->date;
    }

    public function setDate(string $date): void
    {
        $this->date = $date;
    }

    public function getIdBook(): int
    {
        return $this->id_book;
    }

    public function setIdBook(int $id_book): void
    {
        $this->id_book = $id_book;
    }

    public function getIdUsers(): int
    {
        return $this->id_users;
    }

    public function setIdUsers(int $id_users): void
    {
        $this->id_users = $id_users;
    }


}