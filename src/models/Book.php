<?php


class Book
{
    private $title;
    private $description;
    private $image;
    private $id_author;
    private $id;

    public function __construct($title, $description, $image, $id_author, $id)
    {
        $this->title = $title;
        $this->description = $description;
        $this->image = $image;
        $this->id_author = $id_author;
        $this->id =$id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description)
    {
        $this->description = $description;
    }

    public function getImage(): string
    {
        return $this->image;
    }

    public function setImage(string $image)
    {
        $this->image = $image;
    }

    public function getIdAuthor()
    {
        return $this->id_author;
    }

    public function setIdAuthor($id_author): void
    {
        $this->id_author = $id_author;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id): void
    {
        $this->id = $id;
    }


}