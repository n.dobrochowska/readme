<?php


class Bookpage
{
    private $title;
    private $description;
    private $image;
    private $author;
    private $category;

    /**
     * Bookpage constructor.
     * @param $title
     * @param $description
     * @param $image
     * @param $author
     */
    public function __construct(string $title, string $description, string $image, string $author, string $category)
    {
        $this->title = $title;
        $this->description = $description;
        $this->image = $image;
        $this->author = $author;
        $this->category = $category;
    }

    public function getCategory(): string
    {
        return $this->category;
    }

    public function setCategory(string $category): void
    {
        $this->category = $category;
    }


    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title): void
    {
        $this->title = $title;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description): void
    {
        $this->description = $description;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function setImage($image): void
    {
        $this->image = $image;
    }

    public function getAuthor()
    {
        return $this->author;
    }

    public function setAuthor($author): void
    {
        $this->author = $author;
    }



}