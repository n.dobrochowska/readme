<?php
require_once 'AppController.php';
require_once __DIR__.'/../models/Author.php';
require_once __DIR__.'/../repository/AuthorRepository.php';

class AuthorController extends AppController {
    private $message = [];

    public function addAuthorToDb(){
        if(!$this->currentSession()){
            $this->render('login');
        }
        if($this->isPost()){
            $name = $_POST['name'];
            $surname = $_POST['surname'];
            $type = $_SESSION['type'];

            if($type != "admin"){
                $this->message[] = "U dont have permissions to do that";
                return $this->render('addAuthor', ['messages' => $this->message]);
            }

            if($name === ""){
                $this->message[] = "Name is not set";
                return $this->render('addAuthor', ['messages' => $this->message]);
            }

            if($surname === "") {
                $this->message[] = "Surname is not set";
                return $this->render('addAuthor', ['messages' => $this->message]);
            }

            $author_repository = new AuthorRepository();

            if($author_repository->getAuthorByName($name,$surname)){
                $this->message[] = "This Author already Exists";
                return $this->render('addAuthor', ['messages' => $this->message]);
            }

            $author = new Author($name, $surname);

            $author_repository->addAuthor($author);

            $this->message[] = "Success";
            return $this->render('addAuthor', ['messages' => $this->message]);

        }
    }
}