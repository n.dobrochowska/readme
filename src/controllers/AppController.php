<?php

class AppController {

    private $request;

    public function __construct()
    {
        $this->request = $_SERVER['REQUEST_METHOD'];
    }

    protected function isGet(): bool
    {
        return $this->request === 'GET';
    }

    protected function isPost(): bool
    {
        return $this->request === 'POST';
    }

    protected function render(string $template = null, array $variables = [])
    {
        $templatePath = 'public/views/'. $template.'.php';
        $output = 'File not found';

        if(file_exists($templatePath)){
            extract($variables);

            ob_start();
            include $templatePath;
            $output = ob_get_clean();
        }
        print $output;
    }

    protected function currentSession(){
        if(isset($_COOKIE['session'])) {
            if(!session_id()) {
                session_id($_COOKIE['session']);
                session_start();
            }
            if(session_id() != $_COOKIE['session']){
                session_destroy();
                session_id($_COOKIE['session']);
                session_start();
            }
            return true;
        }
        return false;
    }
}