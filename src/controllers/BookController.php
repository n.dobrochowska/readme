<?php
require_once 'AppController.php';
require_once __DIR__.'/../models/Book.php';
require_once __DIR__.'/../repository/BookRepository.php';
require_once __DIR__.'/../repository/BookpageRepository.php';
require_once __DIR__.'/../repository/AuthorRepository.php';
require_once __DIR__.'/../repository/CategoryRepository.php';

class BookController extends AppController
{


    const MAX_FILE_SIZE = 1024 * 1024;
    const SUPPORTED_TYPES = ['image/png', 'image/jpeg'];
    const UPLOAD_DIRECTORY = '/../public/uploads/';

    private $message = [];
    private $categories = [];
    private $authors = [];

    public function projects() {
        if(!$this->currentSession()){
            return $this->render('login');
        }
        $book_repository = new BookRepository();
        $books = $book_repository->getBooks();
        $this->render('projects', ['books' => $books]);
    }

    public function addBookToDb()
    {
        $author_repository = new AuthorRepository();
        $category_repository = new CategoryRepository();
        $this->authors = $author_repository->getAuthors();
        $this->categories = $category_repository->getCategories();
        if (!$this->currentSession()) {
            $this->render('login');
        }
        if ($this->isPost()) {
            $title = $_POST['title'];
            $description = $_POST['description'];
            $image = $_POST['image'];
            $author = $_POST['author'];
            $cat1 = $_POST['cat1'];
            $cat2 = $_POST['cat2'];
            $cat3 = $_POST['cat3'];
            $type = $_SESSION['type'];

            if ($this->isPost() && is_uploaded_file($_FILES['file']['tmp_name']) && $this->validate($_FILES['file'])) {
                move_uploaded_file(
                    $_FILES['file']['tmp_name'],
                    dirname(__DIR__) . self::UPLOAD_DIRECTORY . $_FILES['file']['name']
                );

                if ($type != "admin") {
                    $this->message[] = "U do not have permissions to do that";
                    return $this->render('addBook', ['messages' => $this->message, 'authors' => $this->authors, 'categories' => $this->categories]);
                }

                if ($title === "") {
                    $this->message[] = "Title is not set";
                    return $this->render('addBook', ['messages' => $this->message, 'authors' => $this->authors, 'categories' => $this->categories]);
                }

                if ($description === "") {
                    $this->message[] = "Description is not set";
                    return $this->render('addBook', ['messages' => $this->message, 'authors' => $this->authors, 'categories' => $this->categories]);
                }

                if ($cat1 === "") {
                    $this->message[] = "Required Category is not set";
                    return $this->render('addBook', ['messages' => $this->message, 'authors' => $this->authors, 'categories' => $this->categories]);
                }

                if ($author === "") {
                    $this->message[] = "Author is not set";
                    return $this->render('addBook', ['messages' => $this->message, 'authors' => $this->authors, 'categories' => $this->categories]);
                }


                $book_repository = new BookRepository();

                if ($book_repository->getBookByTitle($title)) {
                    $this->message[] = "This Book already Exists";
                    return $this->render('addBook', ['messages' => $this->message, 'authors' => $this->authors, 'categories' => $this->categories]);
                }

                $author_id = $author_repository->getAuthorId($author);

                $book = new Book($title, $description, $_FILES['file']['name'], $author_id,0);

                $book_repository->addBook($book);

                $new_book_id = $book_repository->getBookId($title);
                $category_repository->addBookToCategory($cat1, $new_book_id);
                if ($cat2 != "" && $cat2 != $cat1){
                    $category_repository->addBookToCategory($cat2, $new_book_id);
                }
                if ($cat3 != "" && $cat3 != $cat1 && $cat3 != $cat2){
                    $category_repository->addBookToCategory($cat3, $new_book_id);
                }

                $this->message[] = "Success";
                return $this->render('addBook', ['messages' => $this->message, 'authors' => $this->authors, 'categories' => $this->categories]);

            }
            return $this->render('addBook', ['messages' => $this->message, 'authors' => $this->authors, 'categories' => $this->categories]);
        }
    }
    private function validate(array $file): bool
    {
        if ($file['size'] > self::MAX_FILE_SIZE) {
            $this->message[] = 'File is too large for destination file system.';
            return false;
        }

        if (!isset($file['type']) || !in_array($file['type'], self::SUPPORTED_TYPES)) {
            $this->message[] = 'File type is not supported.';
            return false;
        }
        return true;
    }

    public function addBook() {
        if(!$this->currentSession()){
            return $this->render('login');
        }
        $author_repository = new AuthorRepository();
        $category_repository = new CategoryRepository();
        $this->authors = $author_repository->getAuthors();
        $this->categories = $category_repository->getCategories();
        return $this->render('addBook',['messages' => $this->message, 'authors' => $this->authors, 'categories' => $this->categories]);    }

    public function search(){
        $contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';

        if ($contentType === "application/json") {
            $content = trim(file_get_contents("php://input"));
            $decoded = json_decode($content, true);

            header('Content-type: application/json');
            http_response_code(200);

            $bookpage_repository = new BookpageRepository();

            echo json_encode($bookpage_repository->getBookByTitle($decoded['search']));
        }
    }
}