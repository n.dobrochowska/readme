<?php

require_once 'AppController.php';
require_once __DIR__.'/../models/User.php';
require_once __DIR__.'/../repository/UserRepository.php';

class SecurityController extends AppController
{
    public function login(){
        if($this->currentSession()){
            $url = "http://$_SERVER[HTTP_HOST]";
            header("Location: {$url}/projects");
        }
        $userRepository = new UserRepository();

        if(!$this->isPost()) {
            return $this->render('login');
        }

        $email = $_POST["email"];
        $password = md5(md5($_POST["password"]));

        $user = $userRepository->getUser($email);

        if (!$user) {
            return $this->render('login', ['messages' => ['User not exist!']]);
        }

        if ($user->getEmail() !== $email) {
            return $this->render('login', ['messages' => ['User with this email not exist!']]);
        }

        if ($user->getPassword() !== $password) {
            return $this->render('login', ['messages' => ['Wrong password!']]);
        }

        if (!$user->isEnabled()) {
            return $this->render('login', ['messages' => ['Your ccount has been blocked']]);
        }

        //session cookies
        session_start();
        $_SESSION['email']=$user->getEmail();
        $_SESSION['type']=$user->getType();
        setcookie("session", session_id(), time() + 7200, "/");
        //return $this->render('projects');

        $url = "http://$_SERVER[HTTP_HOST]";
        header("Location: {$url}/projects");
    }

    public function logout(){
        $this->currentSession();
        unset($_SESSION['']);
        session_unset();
        session_destroy();
        setcookie("session", "", time()-30, "/");

        return $this->render('login');
    }
}