<?php
require_once 'AppController.php';
require_once __DIR__.'/../models/Comment.php';
require_once __DIR__.'/../repository/CommentRepository.php';
require_once __DIR__.'/../repository/UserRepository.php';
require_once __DIR__.'/../models/Bookpage.php';
require_once __DIR__.'/../repository/BookpageRepository.php';
require_once __DIR__.'/../repository/AuthorRepository.php';

class BookpageController extends AppController {
    private $book_id;

    public function bookpage() {
        if(!$this->currentSession()){
            return $this->render('login');
        }
        if($this->isGet()){
            $book_id = $_GET['id'];
        }
        $_SESSION['book_id']=$book_id;
        $bookpage_repository = new BookpageRepository();
        $bookpage = $bookpage_repository->getBookpage($book_id);
        $this->render('bookpage', ['bookpage' => $bookpage]);
    }

    public function addComment(){
        if(!$this->currentSession()){
            $this->render('login');
        }
        if($this->isPost()){
            date_default_timezone_set('Europe/Warsaw');
            $book_id = $_SESSION['bookpage'];
            $user = $_SESSION['email'];
            $date = new DateTime();

            if($_POST['comment'] === ""){
                $url = "http://$_SERVER[HTTP_HOST]";
                header("Location: {$url}/bookpage?id={$_SESSION['book_id']}");
            }

            $user_repository = new UserRepository();

            $comment = new Comment(0, $_POST['comment'], $date->format('Y-m-d H:i:s'), $user_repository->getUserId($user), 1);
        }
        if(!$this->currentSession()){
            return $this->render('login');
        }
        $this->render('bookpage');
    }

}