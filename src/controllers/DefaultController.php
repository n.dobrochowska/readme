<?php

require_once 'AppController.php';

class DefaultController extends AppController {

    public function index() {
        if(!$this->currentSession()){
            return $this->render('login');
        }
        $this->render('browse');
    }

    public function browse() {
        if(!$this->currentSession()){
            return $this->render('login');
        }
        $this->render('browse');
    }


    public function register() {
        if($this->currentSession()){
            return $this->render('browse');
        }
        $this->render('register');
    }

    public function settings() {
        if(!$this->currentSession()){
            return $this->render('login');
        }
        $type = $_SESSION['type'];
        if ($type != "admin") {
            return $this->render('browse');
        }
        $this->render('settings');
    }

    public function addAuthor() {
        if(!$this->currentSession()){
            return $this->render('login');
        }
        $this->render('addAuthor');
    }

}