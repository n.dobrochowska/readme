<?php

require 'Routing.php';

$path = trim($_SERVER['REQUEST_URI'], '/');
$path = parse_url($path, PHP_URL_PATH);

Routing::get('','DefaultController');
Routing::get('projects','BookController');
Routing::get('browse','DefaultController');
Routing::get('settings','DefaultController');
Routing::get('register','DefaultController');
Routing::get('logout','SecurityController');
Routing::get("addAuthor",'DefaultController');
Routing::get("addBook",'BookController');
Routing::post('login','SecurityController');
Routing::post('bookpage','BookpageController');
Routing::post('addComment','BookpageController');
Routing::post('addAuthorToDb', 'AuthorController');
Routing::post('addBookToDb', 'BookController');
Routing::post('search','BookController');

Routing::run($path);